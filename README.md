# Types Of Bail Bonds


1) Cash Bond
Cash bonds require that the bail amount be paid in full. A cash bail holder can be reimbursed for the full amount of bail, however the court may keep some money for administrative costs. It could take several months before the refund is issued. If the defendant fails to appear before the court, a cash bond will be forfeited.

2) Surety Bond
A surety bond, also known as a bail bond, is the best and most convenient method to get out of jail. This bond is used when the defendant cannot pay the full bail amount in cash or doesn't want to risk forfeiture of a cash bonds. Most cases will result in surety bonds being issued by bail bondmen like the Around The Clock <a href="https://goo.gl/maps/zVqtfP5gn8QXEGtc8">San Jose bail bonds</a>. In exchange for this service, the bondsman usually charges a small percentage. The bondsman will have to pay the full bond value in cash to the county if the defendant does not appear in court. A bondsman might hire a bounty hunters to locate the defendant and return them to custody.

3) Personal bonds
A personal bond is a release with a small upfront payment and the expectation that the defendant will appear at the court date. The personal bond is granted only to defendants who are not considered to be a danger to themselves or others. Personal bonds can be costly and inconvenient. They also cost money and time for classes.

4) Property Bond
Property bonds are allowed in some counties. A property bond is a type of bail bond where the defendant gives the court his or her property. The bail amount must be at least twice the value of the property in most cases. The property is subject to a court lien, which allows it to take ownership of the property in the event that the defendant fails to appear in court or forfeits bail. Property bonds can be more difficult than other types of bond because they require a variety of notarized documents as well as appraisals to verify the property's value.
